# DRD Version 3.x for Drupal 8 #

This roadmap should lay out the direction this module is going to take and
specify the building blocks to get there.

## Purpose of the platform ##

DRD in Drupal 7 is already a personal (self-hosted) platform to administer and
monitor any number of Drupal sites that one owns or controls. And all this
without the need for any third party service, which is a major goal that we
always promise to fulfill.

Moving forward, the Drupal 8 version of this platform will be completely
redesigned and written again from scratch. This is because the "old" platform
is hard-coded in many areas, badly documented and not utilizing available APIs
from the framework and therefore is hard to extend or customize.

How that will change is specified in the next chapter.

At the same time, DRD aims to provide more functionality. Where it is focusing
on monitoring tasks up until Drupal 7, this will be extended to administration
in Drupal 8 and moving forward, i.e. installing new modules or updating existing
ones will be supported for deployment strategies implemented as plugins.

## Principle goals ##

- Use as many built-in APIs from Drupal 8 as possible
- This way, the platform becomes extensible and customizable
- The remote part should be as little intrusive as possible
- Security and privacy are major responsibilities
- Utilize Drush and DrupalConsole
- Implement functionality as plugins where possible

## Building blocks ##

The major architecture follows the assumption that there is any number of hosts,
either virtual or dedicated, of which each can have any number of Drupal
installations called cores, of which each provides either a single or multiple
domains.

Those 3 components (host, core, domain) are implemented as fieldable entities
and the UI of the dashboard uses views with filters and customer fields to
provide overviews of those components and to operate on them.

On each level we will support customisable headers for requests that only will
be made against domains. The SSH configuration happens on the host level and
details about the Drupal installation (version, root directory, security model
and deployment strategy) are defined per core.

More details can be found in the data model which is available as a UML diagram
in this directory.

## Plugins ##

The following components will be implemented as plugins:

- Authentication Models
- Security Models
- Deployment Strategies
- Actions

For each area DRD will provide at least one implementation but allows for
custom modules to provide their own ones that can be utilized in DRD without
changing any code.

### Authentication ###

In Drupal 7 authentication was done via shared secrets in combination with
whitelisted IP addresses. Those are both OK but should be implemented as
independent plugins that can be turned on or off individually.

For requests over SSH the module supports all options of SSH authentication,
i.e. PKI or username/password.

Additional plugins could be thought of like e.g. username/password and/or
cookies for all http requests.

### Security ###

In addition to authentication and to protect both authentication and information
exchange from prying eyes we do need a security model. The easiest way would be
this chain:

- If SSL is available, use that
- If SSH and either Drush or DrupalConsole available, use that
- In all other cases use encryption and for this we provide a plugin

### Deployment Strategy ###

This is new for the Drupal 8 version of DRD and is required to install new
modules, themes or libraries or to update existing ones. The strategies that
should be supported, but not limited to, are:

- Download, extract and copy individually.
- Git, if modules are cloned into working copies.
- Drush, if available, either drush dl or drush upc or drush make
- DrupalConsole, if available, their respective commands
- Composer, if available

All these require SSH access to the respective host. Other methods like FTP
or configuring the webserver such that the web server user has write access
to the code directories will NEVER be supported.

### Actions ###

Each action is its own plugin which at the same time will be
- a Drupal rule action
- a Drush command
- a DrupalConsole command
and they always trigger events before and after execution. This way, everything 
that DRD is able to do can be automated through Drupal's rules engine.

## DRD Core Features ##

The following features probably don't qualify for the plugin infrastructure and
are core functionality of DRD.

### Queue ###

By default, each action gets added to the queue and executed from there by
independent instances or requests. Each action should have an option to get
executed directly without the queue.

### Queue Workers ###

The queue gets executed during cron or by other means to work on queues. DRD
will no longer offer that functionality, it is part of core or other modules:

- Core cron - most likely not executed often enough
- Manually e.g. by using Queue UI: https://www.drupal.org/project/queue_ui
- Ultimate cron: https://www.drupal.org/project/ultimate_cron
- GO: https://github.com/VDMi/DirectQueueGo
- NodeJS: https://www.drupal.org/project/nodejs
- Ajax from Browser UI: may require a new module

### Projects ###

Projects and releases are two more entity types that come with DRD. Drupal core
and all modules and themes that were found once on any of the managed domains
will be stored as a project entity and then each version for each of those
projects ever found on one of those domains will be stored as a release
entity linked to the correct project entity. Each currently enabled release
on each domain will also be linked to the matching domain entity.

This way DRD always has a map of all available releases and updates and that
map can be displayed in a view with exposed filters. Then those releases are
actionable, e.g. update or uninstall.

In addition there should be an option to store information about applied
patches on a per core or per domain level depending on where the project lives.

Providing information about installed projects on each domain is done by an
action. Grabbing information about project updates would be best if that's
done once by DRD, as this is the most efficient way to collect that once for
all the monitored installations without them having to enable the update module.

### Remote Information ###

There is information available on remote hosts, cores and domains, that should
be collected and displayed centrally in DRD. Possible information is:

- Status / Requirements
- Messages
- Variables
- Globals
- Blocks

For status, messages and blocks there might be a good reason to do the rendering
remotely as DRD may not have all components available due to different core and
module versions.

We need an efficient way to store that information. This could easily be done
in cache so that we don't need our own data model for that and the information
can always be refreshed by retrieving it again from the remote site.

That way we could also make this extensible such that remote actions can
provide even more information that we currently don't know ourselves yet.

A potential downside of this approach is that we won't have legacy data for
longer term analysis.

### Heartbeat ###

Here we are collecting varous indicators like

- number of entities (user, node, others)
- number of comments
- number of requests
- number of watchdog entries grouped by severity level

### Graphs ###

This is to display the collected heartbeat data for different periods.

### Alerts ###

This is a new component in the Drupal 8 release and the idea is that thresholds
can be defined on a per host, per core or per domain level for any data that
DRD collects about the remote instances, mainly heartbeat data and project
releases.

If any of those thresholds are exceeded, DRD is going to raise an alert and
optionally sends out notifications on various channels like email, IRC or others.

## Remote module ##

The remote module should be as lightweight as possible and ideally comes without
any dependencies. This is important so that the monitoring does have no impact
on the preformance or other behavioue of those remote sites.

This remote module will be available for Drupal 6, 7 and 8.
