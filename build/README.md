# Preparation

Create a symbolic link to the destination directory (DRD module):

```
ln -s ../../web/modules/contrib/drd destination
```

Install required packages:

```
npm install
```

# Usage

Set a watcher

```
gulp watch
```

Compile SASS once

```
gulp css
```
