const PluginError = require('plugin-error'),
  autoprefixer = require('autoprefixer'),
  gulp = require('gulp'),
  gulpif = require('gulp-if'),
  postcss = require('gulp-postcss'),
  cleanCss = require('gulp-clean-css'),
  gulpStylelint = require('@adorade/gulp-stylelint'),
  sass = require('gulp-sass')(require('sass')),
  sourcemaps = require('gulp-sourcemaps'),
  $ = require('gulp-load-plugins')(),
  watch = require('gulp-watch'),
  minimist = require('minimist'),
  postcssInlineSvg = require('postcss-inline-svg'),
  pxtorem = require('postcss-pxtorem'),
  postcssProcessors = [
    postcssInlineSvg({
      removeFill: true,
      paths: []
    }),
    pxtorem({
      propList: ['font', 'font-size', 'line-height', 'letter-spacing', '*margin*', '*padding*'],
      mediaQuery: true
    })
  ],
  config = {
    cssPath: './css',
    fontsPath: './font',
    imgPath: './img',
    jsPath: './js',
    jsFiles: [],
    sassSources: ['sass/drd.scss'],
    sassBasePath: './sass',
  },
  knownOptions = {
    string: 'env',
    default: {env: process.env.NODE_ENV || 'development'}
  },
  options = minimist(process.argv.slice(2), knownOptions),
  sassOptions = {
    outputStyle: gulpif(options.env !== 'production', 'expanded', 'compressed'),
    includePaths: [
      config.sassBasePath,
      config.sassBasePath + '/environment/' + options.env,
    ]
  };

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function () {
  return gulp.src(config.sassSources)
    .pipe(gulpif(options.env !== 'production', sourcemaps.init({})))
    .pipe(sass(sassOptions, {})
      .on('error', function (error) {
        const message = new PluginError('sass', error.messageFormatted).toString();
        process.stderr.write(`${message}\n`);
        process.exitCode=7;
      }))
    .pipe($.postcss(postcssProcessors))
    .pipe(postcss([autoprefixer()], {}))
    .pipe(gulpStylelint({fix: true}))
    .pipe(gulpif(options.env !== 'production', sourcemaps.write('./maps', {})))
    .pipe(gulpif(options.env === 'production', cleanCss()))
    .pipe(gulp.dest(config.cssPath));
});

// Copy the javascript files into our js folder
gulp.task('js-copy', function () {
  return gulp.src(config.jsFiles).pipe(gulp.dest(config.jsPath));
});

// Patch the javascript files
gulp.task('js-patch', function (done) {
  done();
});

// Copy and patch the javascript files
gulp.task('js', gulp.series('js-copy', 'js-patch'));

// Move the img files into our img folder
gulp.task('img', function () {
});

// Move the font files into our font folder
gulp.task('fonts', function () {
});

gulp.task('default', gulp.parallel('fonts', 'img', 'js', 'sass'));
